#! usr/bin/env python3

import json
import sys
from pathlib import Path

import requests
from bs4 import BeautifulSoup

BASE_URL = 'https://myanimelist.net/topanime.php'

def main(args):

    output_dir = Path(__file__).parent / 'mal_data'

    if args:
        output_dir = Path(args[1])

    if not output_dir.exists():
        output_dir.mkdir()
    
    current_page = 0
    row_data = []

    while True:

        r = requests.get(f"{BASE_URL}?limit={current_page}")
        soup = BeautifulSoup(r.text, features='html.parser')

        try:

            for table_row in soup.select("table.top-ranking-table tr.ranking-list"):
                # Each row: td.rank, td.title, td.score, td.your-score, td.status
                # We can ignore the last two rows
                cells = table_row.findAll('td')

                if len(cells) > 0:
                    try:
                        try:
                            rank = int(cells[0].text)
                        except ValueError:
                            print("Last anime reached, saving to file!")
                            raise StopIteration
                        title = cells[1].select_one('div a[class^="hoverinfo_"]').text.strip()
                        avg_score = float(cells[2].text)
                    except ValueError:
                        continue

                    row_data.append({'rank': rank, 'title': title, 'score': avg_score})
                    
        except StopIteration:
            break

        if current_page % 200 == 0 and current_page != 0:
            print(f"{current_page} rows of data read")
        
        current_page += 50

    with open(output_dir / 'raw_data.json', 'w') as f:
        f.write(json.dumps(row_data))

    print("Raw data saved!")

    with open(output_dir / 'worst_to_best.json', 'w') as f:
        f.write(json.dumps(sorted(row_data, key=lambda x: x['score'])))

    print("Sorted data saved!")

if __name__ == '__main__':
    main(sys.argv[1:])
